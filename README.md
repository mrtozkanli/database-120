# README.md

## Percona servers for MySQL 5.7 Installation & Configuration Project

The aim of our project is to install Percona MySql 5.7 with Ansible and complete post installation steps like user creaton,finetunning,replication,logging.
Before starting the project, let's look at a few advantages of percona mysql server.

1. Performance Optimization
2. Advanced Monitoring and Management Tools
3. High Availability and Scalability
4. Open Source and Community Support
5. Consistent Release Schedule

## Installation

Requirements:

1. 2 Ubuntu 22.04 LTS Nodes (I created 2 nodes on GCP.)
2. Source of each node: 2 Core , 4 Gb Memory
3. We need to create a SSH key to access the servers in the public cloud.

Referans document url: https://docs.percona.com/percona-server/5.7/installation/apt_repo.html

- I put the repo and installment steps into two separate tasks. For the installation, I took the official document of Percona MySql as reference and added the link above.

## Finetunning

- We may need to change the configurations on the servers according to different cases that develop in the production environment. Instead of advancing this management process manually, we can do it as-a-code. For this, finetuning yaml. I prepared and prepared a sample. We can increase the number of these custom configs according to changing situations.

## User Create

I completed the user creation task by preparing an ansible playbook with the community module.

![Alt text](image-3.png)

## Replication Master/Slave

We will create a root passwd for master and slave nodes .

```
UPDATE mysql.user SET authentication_string=null WHERE User='root';
FLUSH PRIVILEGES;
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'passwd';
exit;

```

For slave nodes we need to create a user in master node which called "replica"

```
CREATE USER 'replica'@'34.172.94.107' IDENTIFIED WITH mysql_native_password BY 'denizturkmen';
GRANT REPLICATION SLAVE ON *.*TO 'replica'@'34.172.94.107';

```

Master config file:

```
bind-address = 0.0.0.0
server-id = 1
log_bin = /var/log/mysql/mysql-bin.index
```

Slave config file:

```
bind-address = 0.0.0.0
server-id = 2
log_bin = /var/log/mysql/mysql-bin.index
```

```
sudo mysql -uroot -p
STOP SLAVE;
mysql> CHANGE MASTER TO
-> MASTER_HOST='104.154.168.195',
-> MASTER_USER='replica',
-> MASTER_PASSWORD='passwd',
-> MASTER_LOG_FILE='mysql-bin.000299',
-> MASTER_LOG_POS=365;
START SLAVE;

```

## Logging

- I add custom log to collect event. Error log comes with the default installation in logging.yml task
